import { Navigation } from 'react-native-navigation';

import { registerScreens } from './src/screens';

registerScreens(); // this is where you register all of your app's screens

// start the app
Navigation.startTabBasedApp({
  tabs: [
    {
      label: 'Profile',
      screen: 'example.FirstTabScreen', // this is a registered name for a screen
      icon: require('./src/assets/profile.png'),
      title: 'Profile Page'
    },
    {
      label: 'Home',
      screen: 'example.SecondTabScreen',
      icon: require('./src/assets/home.png'),
      title: 'Home Page'
    }
  ]
});