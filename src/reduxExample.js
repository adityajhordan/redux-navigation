import {createStore, combineReducers, applyMiddleware} from 'redux';
import {logger} from 'redux-logger';
import thunk from 'redux-thunk';
import axios from 'axios';
import promise from 'redux-promise-middleware';

const initialState = {
	fetching : false,
	fetched : false,
	error : null,
	data : '',
}

//create reducer
const hero_reducer = function(state=initialState, action){
	switch(action.type){
		case "FETCH_HEROES_PENDING":
			return {...state, fetching:true};
			break;
		case "FETCH_HEROES_FULLFILLED":
			return {...state, fetching:false, data: action.payload};
			break;
		case "FETCH_HEROES_REJECTED":
			return {...state, fetching:false, error: action.payload};
			break;
	}
	return state;
}

//Middleware logger is for information dispatch, promise() is belonging to redux-promise-middleware
const middleware = applyMiddleware(logger, thunk, promise());

//combineReducer
const reducers = combineReducers ({
	hero : hero_reducer
});

//createStore
const store = createStore(reducers, middleware);

//subscriber
store.subscribe(()=>{
	console.log('##RequestAPI Store updated : ', store.getState());
});

//dispatched async need thunk in middleware without promis-middleware
// store.dispatch((dispatch)=>{
// 	dispatch({type: "FETCH_HEROES_PENDING"})

// 	axios.get('https://randomuser.me/api/')
// 	.then((response)=>{
// 		dispatch({type: "FETCH_HEROES_FULLFILLED", payload:response});
// 	})
// 	.catch((err)=>{
// 		dispatch({type: "FETCH_HEROES_ERROR", payload:err});
// 	})
// })


//simpleway dispatch using promise-middleware , automatic update reducer as default_PENDING / _FULLFILLED / _REJECTED
store.dispatch({
	type:"FETCH_HEROES",
	payload: axios.get('https://randomuser.me/api/')
})

